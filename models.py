#!/usr/bin/python3


import sqlalchemy.ext.declarative
import sqlalchemy.orm
import sqlalchemy.event
import sqlalchemy
import datetime


Base = sqlalchemy.ext.declarative.declarative_base()
Session = sqlalchemy.orm.sessionmaker()


class Signup( Base ):
    __tablename__ = 'signup'
    
    # VULNERABILITY: A2 Broken Authentication and Session Management
    # With this column definition the ids are reused
    id = sqlalchemy.Column( sqlalchemy.Integer, primary_key = True )
    # fix
    # __table_args__ = { 'sqlite_autoincrement': True }
    date = sqlalchemy.Column( sqlalchemy.DateTime, nullable = False )
    name = sqlalchemy.Column( sqlalchemy.String( 20 ), nullable = False )
    address = sqlalchemy.Column( sqlalchemy.String( 50 ), nullable = False )
    profile = sqlalchemy.Column( sqlalchemy.String( 100 ), nullable = False, default = '' )

    def __repr__( self ):
        return '<Signup( name = {name}, address = {address})>'.format(
            name = self.name,
            address = self.address
        )


@sqlalchemy.event.listens_for( Signup.__table__, 'after_create' )
def insert_initial_data( *args, **kwargs ):
    session = Session()
    
    session.add( Signup(
        name = 'Participant 1',
        address = 'Address of participant 1',
        date = datetime.datetime( year = 2017, month = 12, day = 1, hour = 10, minute = 24, second = 12 ),
        profile = 'Everyday ninja'
    ) )
    session.add( Signup(
        name = 'Participant 2',
        address = 'Address of participant 2',
        date = datetime.datetime( year = 2017, month = 12, day = 2, hour = 8, minute = 12, second = 7 ),
        profile = 'Couch potato'
    ) )
    session.add( Signup(
        name = 'Participant 3',
        address = 'Address of participant 3',
        date = datetime.datetime( year = 2017, month = 12, day = 3, hour = 23, minute = 50, second = 37 ),
        profile = 'Not disclosed'
    ) )
    session.add( Signup(
        name = 'Participant 4',
        address = 'Address of participant 4',
        date = datetime.datetime( year = 2017, month = 12, day = 4, hour = 6, minute = 57, second = 52 ),
        profile = 'Special agent'
    ) )
    session.commit()


# Initialize an engine
engine = sqlalchemy.create_engine( 'sqlite:///:memory:', echo = True )
# engine = sqlalchemy.create_engine( 'sqlite:///signup.db', echo = True )
Session.configure( bind = engine )
# Create tables
Base.metadata.create_all( engine )
