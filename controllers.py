
import tornado.web
import sqlalchemy.orm.exc
import datetime
import http

import models

COOKIE_LIFETIME_DAYS = 1 / 24 * 2  # 1 day / 24 hours / 


class SignupHandler( tornado.web.RequestHandler ):
    def get( self ):
        return self.render( 'signup.html' )

    def post( self ):
        session = models.Session()
        
        session.add(
            models.Signup( name = self.get_body_argument( 'name', None ),
                           address = self.get_body_argument( 'address', None ),
                           date = datetime.datetime.now(),
                           profile = self.get_body_argument( 'profile', '' ) )
        )
        
        session.commit()
        
        self.redirect( '/confirm' )


class ConfirmHandler( tornado.web.RequestHandler ):
    def get( self ):
        self.render( 'confirm.html' )


class ListHandler( tornado.web.RequestHandler ):
    def get( self ):
        sign_ups = []
        
        session = models.Session()
        for signup in session.query( models.Signup ).order_by( 'date' ):
            sign_ups.append( signup )
        
        self.render( 'list.html', sign_ups = sign_ups )


class SignupEditHandler( tornado.web.RequestHandler ):
    def get_current_user( self ):
        user_id = self.get_secure_cookie( 'user_id', max_age_days = COOKIE_LIFETIME_DAYS )
        
        try:
            user_id = int( user_id )
            
            session = models.Session()
            if session.query( models.Signup ).filter_by( id = user_id ).count() == 0:
                self.clear_cookie( 'user_id' )
                user_id = None
        except:
            user_id = None
        
        return user_id
    
    def is_authenticated( self ):
        return self.get_current_user() is not None
    
    def get( self, user_id ):
        session = models.Session()
        sign_up = session.query( models.Signup ).filter( models.Signup.id == user_id ).first()
        if sign_up is None:
            raise tornado.web.HTTPError( http.HTTPStatus.NOT_FOUND )
        
        self.render( 'profile.html',
                     sign_up = sign_up,
                     logged_in = self.is_authenticated(),
                     logged_in_user_id = self.get_current_user()
        )

    def handle_login( self ):
        name = self.get_body_argument( 'name', None )
        address = self.get_body_argument( 'address', None )
        if None in [name, address]:
            raise tornado.web.HTTPError( status_code = http.HTTPStatus.BAD_REQUEST )
        
        session = models.Session()
        logged_in = session.query( models.Signup ).filter( models.Signup.name == name,
                                                           models.Signup.address == address ).first()
        if logged_in is not None:
            # Valid for abt two hours
            self.set_secure_cookie( 'user_id', str( logged_in.id ), expires_days = COOKIE_LIFETIME_DAYS )
    
        self.redirect( '/signup/{id}'.format( id = logged_in.id ) )
    
    def handle_update( self, user_id ):
        # VULNERABILITY:
        # We just check that the user is authenticated, but fail to verify they're operating only on their
        # own account
        if not self.is_authenticated():
            raise tornado.web.HTTPError( status_code = http.HTTPStatus.UNAUTHORIZED )
        
        name = self.get_body_argument( 'name', None )
        address = self.get_body_argument( 'address', None )
        profile = self.get_body_argument( 'profile', None )
        date = self.get_body_argument( 'date', None )
        
        if name in [ None, '' ]:
            raise tornado.web.HTTPError( status_code = http.HTTPStatus.BAD_REQUEST,
                                         reason = 'Empty name' )
        if address in [ None, '' ]:
            raise tornado.web.HTTPError( status_code = http.HTTPStatus.BAD_REQUEST,
                                         reason = 'Empty address' )
        if profile in [ None, '' ]:
            raise tornado.web.HTTPError( status_code = http.HTTPStatus.BAD_REQUEST,
                                         reason = 'Empty profile' )
        if date is None:
            raise tornado.web.HTTPError( status_code = http.HTTPStatus.BAD_REQUEST,
                                         reason = 'Empty date' )
        try:
            date = datetime.datetime.strptime( date, '%Y-%m-%d %H:%M:%S.%f' )
        except ( ValueError, TypeError ):
            raise tornado.web.HTTPError( status_code = http.HTTPStatus.BAD_REQUEST,
                                         reason = 'Invalid date' )
        
        session = models.Session()
        # Here we're doing a mistake. User submitted user_id is being used rather and logged in users'
        sign_in = session.query( models.Signup ).filter( models.Signup.id == user_id ).first()
        if sign_in is None:
            raise tornado.web.HTTPError( http.HTTPStatus.NOT_FOUND )

        sign_in.name = name
        sign_in.address = address
        sign_in.profile = profile
        sign_in.date = date
        
        session.commit()
        
        self.redirect( self.request.path )
        
    def handle_delete( self, user_id ):
        # VULNERABILITY:
        # Again, we check that user is authenticated, but permit them to operate on accounts other than theirs
        if not self.is_authenticated():
            raise tornado.web.HTTPError( status_code = http.HTTPStatus.UNAUTHORIZED )
        
        session = models.Session()
        
        sign_in = session.query( models.Signup ).filter( models.Signup.id == user_id ).first()
        session.delete( sign_in )
        session.commit()
        
        self.redirect( '/' )
    
    def post( self, user_id ):
        action = self.get_body_argument( 'action', None )
        
        if action == 'log_in':
            self.handle_login()
        elif action == 'update':
            self.handle_update( user_id = user_id )
        elif action == 'delete':
            self.handle_delete( user_id = user_id )
        else:
            raise tornado.web.HTTPError( status_code = http.HTTPStatus.BAD_REQUEST,
                                         reason = 'Submitted form is missing "action"')
