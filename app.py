#!/usr/bin/python3

import tornado.ioloop
import tornado.web
import tornado.httpserver
import tornado
import os

import controllers


class Application( tornado.web.Application ):
    def __init__( self ):
        handlers = [
            ( r'/signup/?', controllers.SignupHandler ),
            ( r'/confirm/?', controllers.ConfirmHandler ),
            ( r'/signup/(.*?)/?', controllers.SignupEditHandler ),
            ( r'/?', controllers.ListHandler ),
        ]
        
        settings = dict(
            debug = True,  # A5 - Security misconfiguration
            autoreload = True,
            autoescape = None,  # A3 - XSS
            template_path = os.path.join( os.path.dirname( __file__ ), 'templates' ),
            cookie_secret = 'KTsJcnQnER*pau(J8yAqt6)ZyU%u=-4"',
            xsrf_cookies = False,  # A8 - CSRF
        )
        
        super( Application, self ).__init__( handlers = handlers, **settings )


if __name__ == '__main__':
    http_server = tornado.httpserver.HTTPServer( Application() )
    http_server.listen( 8080 )
    
    tornado.ioloop.IOLoop.current().start()
