
CYBER SECURITY BASE COURSE PROJECT I
======

An application infested with security issues

## INSTALLATION

  1.  Install Python 3 (Python 2 might work but is untested) and pip (https://pypi.python.org/pypi/pip)
  2.  Create a virtualenv `virtualenv --python=python3 signup-app`
  3.  chdir to the virtualenv folder `cd signup-app`
  4.  Activate the virtual environment `source bin/activate`
  5.  Git clone the project code `git clone https://bitbucket.org/por/signup-app.git`
  6.  chdir to the project folder `cd signup-app`
  7.  Install dependencies `pip install -r requirements.txt`
  8.  Run app `python app.py`
  9.  Follow project brief

## REPORT

### INSTALLATION INSTRUCTIONS AND APPLICATION SOURCES
---
https://bitbucket.org/por/signup-app

### VULNERABILITY 1
A2 - Broken Authentication and Session Management
    (with a touch of A5 - Security misconfiguration)
---

Authentication scheme is broken in many ways, and as maybe the worst, implemented by myself.

Application uses a very light authentication scheme in which users are authenticated via name and address. Address
contains no security mechanisms usually associated with a password in password authentication scheme i.e. hashing.
Cookies are secured from tampering by signing them with HMAC-SHA256 signature, but there's a way to get access to
an account of another (newly registered) user by luring them to sign up at a time useful for the attacker.

Issue: Account "hijack"

Steps to reproduce:

  1. Create an account (http://127.0.0.1:8080/signup), make note of the name and address
  2. Go to your signup information page via the front page
  3. Log in with name and address you gave during sign up
  4. Delete your account
  5. Create a new account (or lure your target to create an account), make note of the name and address
  6. Go to the signup information page of the newly created account. You're logged in and able to edit it's
     information

The main culprit of this issue is the use of simple user_ids as the account and login identifier 
associated with insecure default configuration in SQLite which enables reuse of auto increment columns.

The solution in current state would be to reconfigure SQLite use AUTOINCREMENT attribute for the id column.
As a real solution implement (use an existing) proper server side session management in which only non-guessable
session ids would be handed out to the client. The server would maintain the association between user ids and
session ids, and if a user is deleted, the delete would be cascaded to session ids.

### VULNERABILITY 2
A3 - Cross-Site Scripting (XSS)
     (with a touch of A5 - Security misconfiguration)
---

Application lacks proper escaping of user submitted data, due to which XSS is possible.

Issue: Cross Site Scripting

Steps to reproduce:

  1. Go to account signup page (http://127.0.0.1:8080/signup)
  2. Type in the name field: Name<script>alert( document.cookie );</script>
  3. Fill in address
  4. Type in a profile text
  5. Go to front page (http://127.0.0.1:8080). Now anyone who opens the front page are greeted with an alert
     showing their cookie. alert() can be substituted with any code that submits the cookie to the attackers.

The issue is present because Tornado framework's autoescape is disabled (set to None) in app.py.
It can be fixed in two ways, either by commenting out 'autoescape = None' in app.py settings, or by
explicitly using escape() in output of variables in html files. An example of latter can be found in
list.html where names of 1st three signups are escaped properly.

### VULNERABILITY 3

A5 - Security Misconfiguration

---

Application is laden with security affecting configuration changes without which some of the other
issues mentioned here would be much more difficult, if not impossible, to achieve.

Issue: Debug is enabled

Steps to reproduce:

  1. Browse to http://127.0.0.1:8080/fff
  2. A 404 error with a stack trace is presented

A stack trace gives out a lot of information about directory layout etc. of the web app, and should be disabled
in all environments except actual development environment. In order to enhance security of the application, at
least following settings, given in the app.py configuration, should be reverted back to their more secure defaults:

  * autoescape = None
  * debug = True
  * xsrf_cookies = False
  * autoreload = True


### VULNERABILITY 4

A7 - Missing function level access control

---

Application enables profile updates for registered users. In order to make an update, the user must be logged in.
Login is available on profile page of any user. The update form is shown only on when you view your personal
profile page. If you open any other users' profile, the form is hidden. But, the application fails to properly
authorize profile change requests, and with little effort, it is possible to edit or remove profile of any user.

Following assumes Chrome browser.

Issue: Inadequate validation of profile update requests

Steps to reproduce:

  1. Go to sign up page (http://127.0.0.1:8080/signup)
  2. Sign up for a new account (make note of name and address)
  3. Go to profile page of your newly created profile i.e. http://127.0.0.1:8080/signup/5
  4. Type in name and address given in step 2, and you get redirected to your personal profile page
  5. Right-click on Name edit profile form field, and choose Inspect. In the DOM element tree find the form#update_form element above the name field, and change form action: /signup/1.
  6. Change date input (below name) to: 2017-12-01 10:24:12.628436
  7. Submit the form
  8. You get redirected to /signup/1
  9. Go to front page and you see your name at the top of the list.
  10. Open the page of the account created in Step 2 and delete it.
  11. You've now successfully replaced the first registration and made your way to the qualifying top 3.

There are also other ways to exploit the issue. It is possible to delete any signup by changing the action
URL of delete form in the profile edit.

Issue: Delete any profile

Steps to reproduce:

  *  Repeat steps 1-4 from previous issue.

  5. Right-click on Delete button and choose Inspect
  6. Find form#delete_form in the DOM tree and change action http://127.0.0.1:8080/signup/3.
  7. Submit the form.
  8. Participant 3 is removed

Finally, you can change your registration datetime to make your way into the top 3, because registration date
is exposed as a hidden form field (yes, it's a bit obvious).

Issue: Change registration date

Steps to reproduce:

  *  Repeat steps 1-4 from previous issue.

  5. Right click on Save button and choose Inspect
  6. Edit date value above Save button and set it to 2017-12-01 08:00:00.123456
  7. Submit the form
  8. You're at the top of the list

To fix the issue, proper authorization of update, delete etc. requests is needed. It can be done by verifying
the logged in user, as authenticated by the cookie, is the same user whose profile is being updated, and if
the check fails, reject the action.

### VULNERABILITY 5

A8 - CSRF

---

Finally, CSRF checks have been intentionally disabled in the application, due to which all kinds of cross-site
actions and issues are available.

Issue: Cross-site request forgery

Steps to reproduce:

  0. I assume the attacker has already decided which user to attack and has prepared a site for the target.
  1. Open csrf.html included in the project code.
  2. Press Win 10k� button, after which you get redirected to the participant list page
  3. 1st entry in the list was deleted

To fix this issue re-enable xsrf_cookies in app.py settings, and make sure all forms include
`{# module xsrf_form_html() #}` as indicated by templates/signup.html.
